package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.TestUtils;
import com.ozan.foreignexchangeapi.data.ConversionJpaRepository;
import com.ozan.foreignexchangeapi.exception.DataFixerApiException;
import com.ozan.foreignexchangeapi.model.bo.Conversion;
import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;
import com.ozan.foreignexchangeapi.service.external.DataFixerExternalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

/**
 * @author b.mert.cotuk
 */
@SpringBootTest
class CurrencyConversionServiceImplTest {

    @Spy
    @InjectMocks
    CurrencyConversionServiceImpl currencyConversionServiceImpl;

    @Mock
    DataFixerExternalService dataFixerExternalService;

    @Mock
    ConversionJpaRepository conversionJpaRepository;

    @Test
    void convertAndStoreConversion_success() throws ParseException {

        Conversion inputConversion = TestUtils.createInputConversion();
        Conversion conversion = TestUtils.createConversion();
        LatestRatesResponse latestRatesResponse = TestUtils.createLatestRatesResponse();

        Mockito.when(dataFixerExternalService.callLatestRates(Mockito.any(CurrencyPair.class))).thenReturn(latestRatesResponse);
        Mockito.when(conversionJpaRepository.save(Mockito.any(Conversion.class))).thenReturn(conversion);

        Conversion createdConversion = currencyConversionServiceImpl.convertAndStoreConversion(inputConversion);
        Assertions.assertNotNull(createdConversion.getTransactionId());
        Assertions.assertNotNull(createdConversion.getTransactionDate());
        Assertions.assertEquals(inputConversion.getTargetCurrency(), createdConversion.getTargetCurrency());
        Assertions.assertEquals(inputConversion.getSourceCurrency(), createdConversion.getSourceCurrency());
        Assertions.assertEquals(inputConversion.getSourceAmount(), createdConversion.getSourceAmount());
        Assertions.assertEquals(latestRatesResponse.getRates().get(inputConversion.getTargetCurrency()) * inputConversion.getSourceAmount(), createdConversion.getTargetAmount());
    }

    @Test
    void convertAndStoreConversion_failure() throws ParseException {

        Conversion inputConversion = TestUtils.createInputConversion();
        Conversion conversion = TestUtils.createConversion();
        LatestRatesResponse latestRatesResponse = TestUtils.createLatestRatesResponse();

        Mockito.when(dataFixerExternalService.callLatestRates(Mockito.any(CurrencyPair.class))).thenReturn(latestRatesResponse);
        Mockito.when(conversionJpaRepository.save(Mockito.any(Conversion.class))).thenReturn(conversion);

        Mockito.doThrow(new DataFixerApiException("")).when(dataFixerExternalService).callLatestRates(Mockito.any(CurrencyPair.class));

        Assertions.assertThrows(DataFixerApiException.class, () -> {
            currencyConversionServiceImpl.convertAndStoreConversion(inputConversion);
        });
    }

    @Test
    void retrieveConversionList_success() throws ParseException {

        UUID transactionId = TestUtils.createConversion().getTransactionId();
        Date transactionDate = TestUtils.createDate();
        Integer pageNo = 0;
        Integer pageSize = 10;

        Page<Conversion> pageOfConversions = TestUtils.createPageOfConversions();
        Mockito.when(conversionJpaRepository.findAll(Mockito.any(Example.class), Mockito.any(Pageable.class))).thenReturn(pageOfConversions);

        Page<Conversion> results = currencyConversionServiceImpl.retrieveConversionList(transactionId.toString(), transactionDate, pageNo, pageSize);
        Assertions.assertEquals(pageOfConversions, results);
    }

    @Test
    void retrieveConversionList_failure() throws ParseException {

        UUID transactionId = TestUtils.createConversion().getTransactionId();
        Date transactionDate = TestUtils.createDate();
        Integer pageNo = 0;
        Integer pageSize = 10;

        Page<Conversion> pageOfConversions = TestUtils.createPageOfConversions();
        Mockito.when(conversionJpaRepository.findAll(Mockito.any(Example.class), Mockito.any(Pageable.class))).thenReturn(pageOfConversions);

        Page<Conversion> results = currencyConversionServiceImpl.retrieveConversionList(transactionId.toString(), transactionDate, pageNo, pageSize);
        Assertions.assertEquals(pageOfConversions, results);
    }
}