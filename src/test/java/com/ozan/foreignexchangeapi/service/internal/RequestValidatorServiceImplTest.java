package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.TestUtils;
import com.ozan.foreignexchangeapi.exception.MissingParameterException;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author b.mert.cotuk
 */
@SpringBootTest
class RequestValidatorServiceImplTest {

    @Spy
    @InjectMocks
    RequestValidatorServiceImpl requestValidatorServiceImpl;

    ConversionRequest conversionRequest;

    @BeforeEach
    void setUp() {
        conversionRequest = TestUtils.createConversionRequest();
    }

    @Test
    void validateConversionRequest_success() {
        Assertions.assertDoesNotThrow(() -> requestValidatorServiceImpl.validateConversionRequest(conversionRequest));
    }

    @Test
    void validateConversionRequest_fail_on_request_body() {
        conversionRequest = null;
        Assertions.assertThrows(MissingParameterException.class, () -> {
            requestValidatorServiceImpl.validateConversionRequest(conversionRequest);
        });
    }

    @Test
    void validateConversionRequest_fail_on_source_currency() {
        conversionRequest.setSourceCurrency(null);
        Assertions.assertThrows(MissingParameterException.class, () -> {
            requestValidatorServiceImpl.validateConversionRequest(conversionRequest);
        });

    }

    @Test
    void validateConversionRequest_fail_on_target_currency() {
        conversionRequest.setTargetCurrency(null);
        Assertions.assertThrows(MissingParameterException.class, () -> {
            requestValidatorServiceImpl.validateConversionRequest(conversionRequest);
        });
    }

    @Test
    void validateConversionRequest_fail_on_source_amount() {
        conversionRequest.setSourceAmount(null);
        Assertions.assertThrows(MissingParameterException.class, () -> {
            requestValidatorServiceImpl.validateConversionRequest(conversionRequest);
        });
    }

    @Test
    void validateConversionListRequest_success() {
        Assertions.assertDoesNotThrow(() -> requestValidatorServiceImpl.validateConversionListRequest(null, new Date()));
    }

    @Test
    void validateConversionListRequest_fail_on_missing_query_parameters() {
        Assertions.assertThrows(MissingParameterException.class, () -> {
            requestValidatorServiceImpl.validateConversionListRequest(null, null);
        });
    }
}