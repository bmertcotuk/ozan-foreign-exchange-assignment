package com.ozan.foreignexchangeapi.util;

import com.ozan.foreignexchangeapi.TestUtils;
import com.ozan.foreignexchangeapi.exception.InvalidCurrencyPairException;
import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ExchangeRateResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author b.mert.cotuk
 */
@SpringBootTest
class CommonUtilsTest {

    CurrencyPair currencyPair;
    String validCurrencyPairString = "EUR,USD";
    String inValidCurrencyPairString = "EUR,USD,ASASD";

    @BeforeEach
    void setUp() {
        currencyPair = TestUtils.createCurrencyPair();
    }

    @Test
    void getCurrencyPair_success() {
        CurrencyPair currencyPair2 = CommonUtils.getCurrencyPair(validCurrencyPairString);
        Assertions.assertEquals(currencyPair, currencyPair2);
    }

    @Test
    void getCurrencyPair_failure() {

        Assertions.assertThrows(InvalidCurrencyPairException.class, () -> {
            CommonUtils.getCurrencyPair(inValidCurrencyPairString);
        });
    }

    @Test
    void exchangeRateFromLatestRatesResponse() {

        LatestRatesResponse latestRatesResponse = TestUtils.createLatestRatesResponse();

        ExchangeRateResponse exchangeRateResponse = new ExchangeRateResponse();
        exchangeRateResponse.setExchangeRate(latestRatesResponse.getRates().get("USD"));

        ExchangeRateResponse exchangeRateResponse2 = CommonUtils.exchangeRateFromLatestRatesResponse(latestRatesResponse, currencyPair);
        Assertions.assertEquals(exchangeRateResponse, exchangeRateResponse2);
    }
}