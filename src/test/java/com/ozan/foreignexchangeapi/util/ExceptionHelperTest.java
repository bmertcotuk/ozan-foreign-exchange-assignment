package com.ozan.foreignexchangeapi.util;

import com.ozan.foreignexchangeapi.controller.CurrencyConversionController;
import com.ozan.foreignexchangeapi.controller.CurrencyConversionControllerImpl;
import com.ozan.foreignexchangeapi.controller.ExchangeRateController;
import com.ozan.foreignexchangeapi.controller.ExchangeRateControllerImpl;
import com.ozan.foreignexchangeapi.exception.InvalidCurrencyPairException;
import com.ozan.foreignexchangeapi.exception.MissingParameterException;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author b.mert.cotuk
 */
@SpringBootTest
class ExceptionHelperTest {

    private MockMvc mockMvcCurrencyConversionController;
    private MockMvc mockMvcCurrencyExchangeRateController;

    @Mock
    CurrencyConversionControllerImpl currencyConversionController;

    @Mock
    ExchangeRateControllerImpl exchangeRateController;

    @BeforeEach
    public void setup() {
        this.mockMvcCurrencyConversionController = MockMvcBuilders.standaloneSetup(currencyConversionController)
                .setControllerAdvice(new ExceptionHelper())
                .build();

        this.mockMvcCurrencyExchangeRateController = MockMvcBuilders.standaloneSetup(exchangeRateController)
                .setControllerAdvice(new ExceptionHelper())
                .build();
    }

    @Test
    void handleInvalidCurrencyPairException() throws Exception {
        Mockito.when(exchangeRateController.getExchangeRate(Mockito.anyString())).thenThrow(new InvalidCurrencyPairException());
        mockMvcCurrencyExchangeRateController.perform(get("/exchangeRate")
                .param("currencyPair", "EUR,USD,ASDAASDA"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void handleMissingParameterException() throws Exception {
        Mockito.when(currencyConversionController.postConversion(Mockito.any(ConversionRequest.class))).thenThrow(new MissingParameterException(""));
        mockMvcCurrencyConversionController.perform(post("/conversion")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"sourceCurrency\": \"EUR\",\n" +
                        "\t\"sourceAmount\": 12\n" +
                        "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void handleDataFixerApiException() throws Exception {
        Mockito.when(exchangeRateController.getExchangeRate(Mockito.anyString())).thenThrow(new InvalidCurrencyPairException());
        mockMvcCurrencyExchangeRateController.perform(get("/exchangeRate")
                .param("currencyPair", "USD,EUR"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void handleOtherException() throws Exception {
        Mockito.when(currencyConversionController.postConversion(Mockito.any(ConversionRequest.class))).thenThrow(new MissingParameterException(""));
        mockMvcCurrencyConversionController.perform(post("/conversion"))
                .andExpect(status().isInternalServerError());
    }
}