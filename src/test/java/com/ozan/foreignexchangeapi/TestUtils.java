package com.ozan.foreignexchangeapi;

import com.ozan.foreignexchangeapi.model.bo.Conversion;
import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionSimpleResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author b.mert.cotuk
 */
public class TestUtils {

    private static final UUID uuid1 = UUID.randomUUID();
    private static final UUID uuid2 = UUID.randomUUID();
    private static final UUID uuid3 = UUID.randomUUID();

    public static LatestRatesResponse createLatestRatesResponse() {
        LatestRatesResponse latestRatesResponse = new LatestRatesResponse();
        Map rates = new HashMap<String, Double>();
        rates.put("USD", 1.5);
        latestRatesResponse.setRates(rates);
        return latestRatesResponse;
    }

    public static CurrencyPair createCurrencyPair() {
        CurrencyPair currencyPair = new CurrencyPair();
        currencyPair.setBaseCurrency("EUR");
        currencyPair.setTargetCurrency("USD");
        return currencyPair;
    }

    public static Conversion createInputConversion() {
        Conversion conversion = new Conversion();
        conversion.setSourceAmount(100.0);
        conversion.setSourceCurrency("EUR");
        conversion.setTargetCurrency("USD");
        return conversion;
    }

    public static Conversion createConversion() throws ParseException {
        Conversion conversion = new Conversion();
        conversion.setTransactionId(uuid1);
        conversion.setTransactionDate(createDate());
        conversion.setInternalId(101L);
        conversion.setSourceAmount(100.0);
        conversion.setSourceCurrency("EUR");
        conversion.setTargetAmount(150.0);
        conversion.setTargetCurrency("USD");
        return conversion;
    }

    public static Conversion createConversion2() throws ParseException {
        Conversion conversion = new Conversion();
        conversion.setTransactionId(uuid2);
        conversion.setTransactionDate(createDate());
        conversion.setInternalId(102L);
        conversion.setSourceAmount(100.0);
        conversion.setSourceCurrency("EUR");
        conversion.setTargetAmount(150.0);
        conversion.setTargetCurrency("GBP");
        return conversion;
    }

    public static Conversion createConversion3() throws ParseException {
        Conversion conversion = new Conversion();
        conversion.setTransactionId(uuid1);
        conversion.setTransactionDate(createDate());
        conversion.setInternalId(103L);
        conversion.setSourceAmount(100.0);
        conversion.setSourceCurrency("EUR");
        conversion.setTargetAmount(150.0);
        conversion.setTargetCurrency("TRY");
        return conversion;
    }

    public static ConversionResponse createConversionResponse() throws ParseException {
        ConversionResponse conversionResponse = new ConversionResponse();
        conversionResponse.setTransactionId(uuid1);
        conversionResponse.setTransactionDate(createDate());
        conversionResponse.setSourceAmount(100.0);
        conversionResponse.setSourceCurrency("EUR");
        conversionResponse.setTargetAmount(150.0);
        conversionResponse.setTargetCurrency("USD");
        return conversionResponse;
    }

    public static ConversionResponse createConversionResponse2() throws ParseException {
        ConversionResponse conversionResponse = new ConversionResponse();
        conversionResponse.setTransactionId(uuid2);
        conversionResponse.setTransactionDate(createDate());
        conversionResponse.setSourceAmount(100.0);
        conversionResponse.setSourceCurrency("EUR");
        conversionResponse.setTargetAmount(150.0);
        conversionResponse.setTargetCurrency("GBP");
        return conversionResponse;
    }

    public static ConversionResponse createConversionResponse3() throws ParseException {
        ConversionResponse conversionResponse = new ConversionResponse();
        conversionResponse.setTransactionId(uuid1);
        conversionResponse.setTransactionDate(createDate());
        conversionResponse.setSourceAmount(100.0);
        conversionResponse.setSourceCurrency("EUR");
        conversionResponse.setTargetAmount(150.0);
        conversionResponse.setTargetCurrency("TRY");
        return conversionResponse;
    }

    public static List<Conversion> createListOfConversions() throws ParseException {
        return Arrays.asList(createConversion(), createConversion2(), createConversion3());
    }

    public static Page<Conversion> createPageOfConversions() throws ParseException {
        List<Conversion> listOfConversions = createListOfConversions();
        return new PageImpl<>(listOfConversions);
    }

    public static Date createDate() throws ParseException {
        String inputString = "05-11-2020";
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse(inputString);
        return date;
    }

    public static List<ConversionResponse> createListOfConversionResponses() throws ParseException {
        return Arrays.asList(createConversionResponse(), createConversionResponse2(), createConversionResponse3());
    }

    public static Page<ConversionResponse> createPageOfConversionResponses() throws ParseException {
        List<ConversionResponse> listOfConversionResponses = createListOfConversionResponses();
        return new PageImpl<>(listOfConversionResponses);
    }

    public static Conversion createConversionAsRequest() {
        Conversion conversion = new Conversion();
        conversion.setSourceAmount(100.0);
        conversion.setSourceCurrency("EUR");
        conversion.setTargetCurrency("USD");
        return conversion;
    }

    public static ConversionRequest createConversionRequest() {
        ConversionRequest conversionRequest = new ConversionRequest();
        conversionRequest.setSourceAmount(100.0);
        conversionRequest.setSourceCurrency("EUR");
        conversionRequest.setTargetCurrency("USD");
        return conversionRequest;
    }

    public static ConversionSimpleResponse createConversionSimpleResponse() {
        ConversionSimpleResponse conversionSimpleResponse = new ConversionSimpleResponse();
        conversionSimpleResponse.setTransactionId(uuid1);
        conversionSimpleResponse.setTargetAmount(150.0);
        return conversionSimpleResponse;
    }
}
