package com.ozan.foreignexchangeapi.controller;

import com.ozan.foreignexchangeapi.TestUtils;
import com.ozan.foreignexchangeapi.exception.MissingParameterException;
import com.ozan.foreignexchangeapi.mapper.ConversionMapper;
import com.ozan.foreignexchangeapi.model.bo.Conversion;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionSimpleResponse;
import com.ozan.foreignexchangeapi.service.internal.CurrencyConversionService;
import com.ozan.foreignexchangeapi.service.internal.RequestValidatorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;


@SpringBootTest
@ContextConfiguration
@AutoConfigureMockMvc
public class CurrencyConversionControllerImplTest {

    @Spy
    @InjectMocks
    private CurrencyConversionControllerImpl currencyConversionControllerImpl;

    @Mock
    CurrencyConversionService currencyConversionService;

    @Mock
    RequestValidatorService requestValidatorService;

    @Mock
    ConversionMapper conversionMapper;

    Conversion conversionAsRequest;
    Conversion conversionCreated;
    ConversionSimpleResponse conversionSimpleResponse;
    ConversionRequest conversionRequest;
    ConversionResponse conversionResponse;
    ConversionResponse conversionResponse2;
    ConversionResponse conversionResponse3;

    @BeforeEach
    void setUp() throws ParseException {
        conversionAsRequest = TestUtils.createConversionAsRequest();
        conversionCreated = TestUtils.createConversion();
        conversionSimpleResponse = TestUtils.createConversionSimpleResponse();
        conversionRequest = TestUtils.createConversionRequest();
        conversionResponse = TestUtils.createConversionResponse();
        conversionResponse2 = TestUtils.createConversionResponse2();
        conversionResponse3 = TestUtils.createConversionResponse3();

        Mockito.when(conversionMapper.mapToEntity(Mockito.any(ConversionRequest.class))).thenReturn(conversionAsRequest);
        Mockito.when(currencyConversionService.convertAndStoreConversion(Mockito.any(Conversion.class))).thenReturn(conversionCreated);
        Mockito.when(conversionMapper.mapToSimpleResponse(Mockito.any(Conversion.class))).thenReturn(conversionSimpleResponse);
        Mockito.when(conversionMapper.mapToResponse(Mockito.any(Conversion.class))).thenReturn(conversionResponse).thenReturn(conversionResponse2).thenReturn(conversionResponse3);
    }

    @Test
    void postConversion_success() {

        ResponseEntity<ConversionSimpleResponse> responseEntity = currencyConversionControllerImpl.postConversion(conversionRequest);
        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertEquals(conversionSimpleResponse, responseEntity.getBody());
    }

    @Test
    void postConversion_failure() {

        Mockito.doThrow(new MissingParameterException("")).when(requestValidatorService).validateConversionRequest(Mockito.any(ConversionRequest.class));

        Assertions.assertThrows(MissingParameterException.class, () -> {
            ResponseEntity<ConversionSimpleResponse> responseEntity = currencyConversionControllerImpl.postConversion(conversionRequest);
        });
    }

    @Test
    void getConversionList_success() throws ParseException {

        UUID transactionId = conversionCreated.getTransactionId();
        Date transactionDate = TestUtils.createDate();
        Integer pageNo = 0;
        Integer pageSize = 10;
        Page<Conversion> pageOfConversions = TestUtils.createPageOfConversions();
        Page<ConversionResponse> pageOfConversionResponses = TestUtils.createPageOfConversionResponses();

        Mockito.when(currencyConversionService.retrieveConversionList(Mockito.anyString(), Mockito.any(Date.class), Mockito.anyInt(), Mockito.anyInt())).thenReturn(pageOfConversions);


        ResponseEntity<Page<ConversionResponse>> responseEntity = currencyConversionControllerImpl.getConversionList(transactionId.toString(), transactionDate, pageNo, pageSize);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals(pageOfConversionResponses, responseEntity.getBody());
    }

    @Test
    void getConversionList_failure() throws ParseException {

        UUID transactionId = conversionCreated.getTransactionId();
        Date transactionDate = TestUtils.createDate();
        Integer pageNo = 0;
        Integer pageSize = 10;
        Page<Conversion> pageOfConversions = TestUtils.createPageOfConversions();
        Page<ConversionResponse> pageOfConversionResponses = TestUtils.createPageOfConversionResponses();

        Mockito.when(currencyConversionService.retrieveConversionList(Mockito.anyString(), Mockito.any(Date.class), Mockito.anyInt(), Mockito.anyInt())).thenReturn(pageOfConversions);

        Mockito.doThrow(new MissingParameterException("")).when(requestValidatorService).validateConversionListRequest(Mockito.anyString(), Mockito.any(Date.class));


        Assertions.assertThrows(MissingParameterException.class, () -> {
            currencyConversionControllerImpl.getConversionList(transactionId.toString(), transactionDate, pageNo, pageSize);
        });

    }
}