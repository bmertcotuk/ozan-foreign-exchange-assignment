package com.ozan.foreignexchangeapi.controller;

import com.ozan.foreignexchangeapi.exception.DataFixerApiException;
import com.ozan.foreignexchangeapi.model.dto.internal.ExchangeRateResponse;
import com.ozan.foreignexchangeapi.service.internal.ExchangeRateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author b.mert.cotuk
 */
@SpringBootTest
@ContextConfiguration
@AutoConfigureMockMvc
class ExchangeRateControllerImplTest {

    @Spy
    @InjectMocks
    ExchangeRateControllerImpl exchangeRateControllerImpl;

    @Mock
    ExchangeRateService exchangeRateService;


    @Test
    void getExchangeRate_success() {

        ExchangeRateResponse exchangeRateResponse = new ExchangeRateResponse();
        exchangeRateResponse.setExchangeRate(222.2);
        Mockito.when(exchangeRateService.getExchangeRateForCurrencyPair(Mockito.anyString())).thenReturn(exchangeRateResponse);

        ResponseEntity<ExchangeRateResponse> responseEntity = exchangeRateControllerImpl.getExchangeRate("EUR,USD");
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertEquals(exchangeRateResponse, responseEntity.getBody());
    }

    @Test
    void getExchangeRate_failure() {

        ExchangeRateResponse exchangeRateResponse = new ExchangeRateResponse();
        exchangeRateResponse.setExchangeRate(222.2);
        Mockito.doThrow(new DataFixerApiException("")).when(exchangeRateService).getExchangeRateForCurrencyPair(Mockito.anyString());

        Assertions.assertThrows(DataFixerApiException.class, () -> {
            exchangeRateControllerImpl.getExchangeRate("EUR,USD,ASDF");
        });
    }
}