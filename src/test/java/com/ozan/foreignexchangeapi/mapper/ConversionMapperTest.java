package com.ozan.foreignexchangeapi.mapper;

import com.ozan.foreignexchangeapi.TestUtils;
import com.ozan.foreignexchangeapi.model.bo.Conversion;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionSimpleResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;

/**
 * @author b.mert.cotuk
 */
@SpringBootTest
class ConversionMapperTest {

    @Spy
    @InjectMocks
    ConversionMapperImpl conversionMapperImpl;

    Conversion conversion;
    ConversionRequest conversionRequest;
    ConversionResponse conversionResponse;
    ConversionSimpleResponse conversionSimpleResponse;

    @BeforeEach
    void setUp() throws ParseException {
        conversion = TestUtils.createConversion();
        conversionRequest = TestUtils.createConversionRequest();
        conversionResponse = TestUtils.createConversionResponse();
        conversionSimpleResponse = TestUtils.createConversionSimpleResponse();
    }

    @Test
    void mapToResponse() {
        ConversionResponse conversionResponse2 = conversionMapperImpl.mapToResponse(conversion);
        Assertions.assertEquals(conversionResponse, conversionResponse2);
    }

    @Test
    void mapToSimpleResponse() {
        ConversionSimpleResponse conversionSimpleResponse2 = conversionMapperImpl.mapToSimpleResponse(conversion);
        Assertions.assertEquals(conversionSimpleResponse, conversionSimpleResponse2);
    }

    @Test
    void mapToEntity() {
        conversion.setInternalId(null);
        conversion.setTransactionId(null);
        conversion.setTransactionDate(null);
        conversion.setTargetAmount(null);

        Conversion conversion2 = conversionMapperImpl.mapToEntity(conversionRequest);
        Assertions.assertEquals(this.conversion, conversion2);
    }
}