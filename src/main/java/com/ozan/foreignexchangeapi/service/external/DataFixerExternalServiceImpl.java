package com.ozan.foreignexchangeapi.service.external;

import com.ozan.foreignexchangeapi.enums.DataFixerError;
import com.ozan.foreignexchangeapi.exception.DataFixerApiException;
import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;

/**
 * The service implementation for Data Fixer API calls.
 *
 * @author b.mert.cotuk
 */
@Service
public class DataFixerExternalServiceImpl implements DataFixerExternalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataFixerExternalServiceImpl.class);

    private static final String URL = "http://data.fixer.io/api";
    private static final String ACCESS_KEY_NAME = "access_key";
    private static final String ACCESS_KEY_VALUE = "d12af89b67a46e3f3f4f4c845b73279d";
    private static final String BASE_NAME = "base";
    private static final String SYMBOLS_NAME = "symbols";

    /**
     * {@inheritDoc}
     */

    @Override
    public LatestRatesResponse callLatestRates(CurrencyPair currencyPair) {

        LOGGER.info("Calling the external Data Fixer API for currency pair: {}", currencyPair);
        URI uri = UriComponentsBuilder
                .fromUriString(URL + "/latest")
                .queryParam(ACCESS_KEY_NAME, ACCESS_KEY_VALUE)
                .queryParam(BASE_NAME, currencyPair.getBaseCurrency())
                .queryParam(SYMBOLS_NAME, Arrays.asList(currencyPair.getTargetCurrency()))
                .build()
                .toUri();
        RestTemplate restTemplate = new RestTemplate();

        LatestRatesResponse response = restTemplate.getForObject(uri, LatestRatesResponse.class);

        if (!response.isSuccess()) {
            Long code = response.getError().getCode();
            LOGGER.warn("Response from Data Fixer API is not successful. The error code is {}", code);
            DataFixerError dataFixerError = DataFixerError.fromCode(code);
            throw new DataFixerApiException(dataFixerError.getDescription());
        }

        return response;
    }
}
