package com.ozan.foreignexchangeapi.service.external;

import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;

/**
 * The service interface for Data Fixer API calls.
 *
 * @author b.mert.cotuk
 */
public interface DataFixerExternalService {

    /**
     * Calls the latest rates of Data Fixer API with the currency pair input and returns the response. Throws an error if the response is not successful.
     *
     * @param currencyPair
     * @return
     */
    LatestRatesResponse callLatestRates(CurrencyPair currencyPair);
}
