package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ExchangeRateResponse;
import com.ozan.foreignexchangeapi.service.external.DataFixerExternalService;
import com.ozan.foreignexchangeapi.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The service implementation for all exchange rate operations.
 *
 * @author b.mert.cotuk
 */
@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateServiceImpl.class);

    @Autowired
    DataFixerExternalService dataFixerExternalService;

    /**
     * {@inheritDoc}
     */
    @Override
    public ExchangeRateResponse getExchangeRateForCurrencyPair(String currencyPairString) {

        CurrencyPair currencyPair = CommonUtils.getCurrencyPair(currencyPairString);

        LatestRatesResponse externalExchangeRateResponse = dataFixerExternalService.callLatestRates(currencyPair);
        LOGGER.debug("Response from Data Fixer API is {}", externalExchangeRateResponse);

        return CommonUtils.exchangeRateFromLatestRatesResponse(externalExchangeRateResponse, currencyPair);
    }
}
