package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.model.bo.Conversion;
import org.springframework.data.domain.Page;

import java.util.Date;

/**
 * The service interface for all currency conversions operations.
 *
 * @author b.mert.cotuk
 */
public interface CurrencyConversionService {

    /**
     * Calls the Data Fixer API to retrieve the latest rate between the currencies. Multiplies this rate with the amount of source currency to have the target amount. Enriches the conversion with a UUID transaction id and a transaction date. Stores the conversion in database. Errors are thrown if the request body is missing/invalid.
     *
     * @param inputConversion
     * @return persisted conversion
     */
    Conversion convertAndStoreConversion(Conversion inputConversion);

    /**
     * Queries the database to retrieve the conversion records page by page and returns the results as a page.
     *
     * @param transactionId
     * @param transactionDate
     * @param pageNo
     * @param pageSize
     * @return page of conversions
     */
    Page<Conversion> retrieveConversionList(String transactionId, Date transactionDate, Integer pageNo, Integer pageSize);
}
