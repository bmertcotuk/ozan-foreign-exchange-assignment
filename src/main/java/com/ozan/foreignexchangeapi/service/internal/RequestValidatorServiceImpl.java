package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.exception.MissingParameterException;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * The service implementation for all request validations.
 *
 * @author b.mert.cotuk
 */
@Service
public class RequestValidatorServiceImpl implements RequestValidatorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestValidatorServiceImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateConversionRequest(ConversionRequest conversionRequest) {

        LOGGER.info("Validating the post conversion request.");
        if (conversionRequest == null) {
            throw new MissingParameterException("Conversion request body cannot be null");
        }
        if (conversionRequest.getSourceCurrency() == null) {
            throw new MissingParameterException("Field 'sourceCurrency' cannot be null in the conversion request body.");
        }
        if (conversionRequest.getTargetCurrency() == null) {
            throw new MissingParameterException("Field 'targetCurrency' cannot be null in the conversion request body.");
        }
        if (conversionRequest.getSourceAmount() == null) {
            throw new MissingParameterException("Field 'sourceAmount' cannot be null in the conversion request body.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateConversionListRequest(String transactionId, Date transactionDate) {

        LOGGER.info("Validating the conversion list retrieval query parameters.");
        if (transactionId == null && transactionDate == null) {
            throw new MissingParameterException("Either transaction id or transaction date should be provided.");
        }
    }
}
