package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.model.dto.internal.ExchangeRateResponse;

/**
 * The service interface for all exchange rate operations.
 *
 * @author b.mert.cotuk
 */
public interface ExchangeRateService {

    /**
     * Calls Data Fixer API service and returns the exchange rate between the currencies using the comma separated query parameter (e.g. currencyPair=EUR,USD).
     *
     * @param currencyPairString
     * @return exchange rate information
     */
    ExchangeRateResponse getExchangeRateForCurrencyPair(String currencyPairString);
}
