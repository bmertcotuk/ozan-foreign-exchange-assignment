package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.data.ConversionJpaRepository;
import com.ozan.foreignexchangeapi.model.bo.Conversion;
import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;
import com.ozan.foreignexchangeapi.service.external.DataFixerExternalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * The service implementation for all currency conversions operations.
 *
 * @author b.mert.cotuk
 */
@Service
public class CurrencyConversionServiceImpl implements CurrencyConversionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyConversionServiceImpl.class);

    @Autowired
    DataFixerExternalService dataFixerExternalService;

    @Autowired
    ConversionJpaRepository conversionJpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public Conversion convertAndStoreConversion(Conversion inputConversion) {

        CurrencyPair currencyPair = new CurrencyPair(inputConversion.getSourceCurrency(), inputConversion.getTargetCurrency());
        LatestRatesResponse latestRatesResponse = dataFixerExternalService.callLatestRates(currencyPair);
        Double rate = latestRatesResponse.getRates().get(inputConversion.getTargetCurrency());
        LOGGER.info("Calculating the target amount using the rate {}", rate);
        inputConversion.setTargetAmount(rate * inputConversion.getSourceAmount());
        inputConversion.setTransactionDate(new Date());
        inputConversion.setTransactionId(UUID.randomUUID());
        return conversionJpaRepository.save(inputConversion);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<Conversion> retrieveConversionList(String transactionId, Date transactionDate, Integer pageNo, Integer pageSize) {

        // primary key is not exposed, transaction id is still unique as UUID
        LOGGER.info("Retrieving the conversion list.");
        Conversion conversionExample = new Conversion();
        conversionExample.setTransactionId(transactionId == null ? null : UUID.fromString(transactionId));
        conversionExample.setTransactionDate(transactionDate);
        Pageable paging = PageRequest.of(pageNo, pageSize);
        return conversionJpaRepository.findAll(Example.of(conversionExample), paging);
    }

}
