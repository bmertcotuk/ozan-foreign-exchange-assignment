package com.ozan.foreignexchangeapi.service.internal;

import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;

import java.util.Date;

/**
 * The service interface for all request validations.
 *
 * @author b.mert.cotuk
 */
public interface RequestValidatorService {

    /**
     * Throws an error if any of the following is null: request body, source currency, source amount, or target currency
     *
     * @param conversionRequest
     */
    void validateConversionRequest(ConversionRequest conversionRequest);

    /**
     * Throws an error if both transaction id and transaction date are null.
     *
     * @param transactionId
     * @param transactionDate
     */
    void validateConversionListRequest(String transactionId, Date transactionDate);
}
