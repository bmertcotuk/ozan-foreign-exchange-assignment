package com.ozan.foreignexchangeapi.model.dto.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ozan.foreignexchangeapi.enums.ApplicationError;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A response object for all kinds of application errors. Null values are excluded.
 *
 * @author b.mert.cotuk
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {

    private Integer code;
    private String description;
    private String message;

    public ErrorResponse(ApplicationError applicationError, String message) {
        this.code = applicationError.getCode();
        this.description = applicationError.getDescription();
        this.message = message;
    }
}
