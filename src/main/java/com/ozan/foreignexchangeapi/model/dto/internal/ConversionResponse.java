package com.ozan.foreignexchangeapi.model.dto.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * A response object for currency conversion. Null values are excluded.
 *
 * @author b.mert.cotuk
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConversionResponse {

    private UUID transactionId;
    private Date transactionDate;
    private String sourceCurrency;
    private Double sourceAmount;
    private String targetCurrency;
    private Double targetAmount;
}
