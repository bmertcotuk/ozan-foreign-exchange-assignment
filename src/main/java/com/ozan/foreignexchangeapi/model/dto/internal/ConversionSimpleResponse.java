package com.ozan.foreignexchangeapi.model.dto.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.UUID;

/**
 * A simple version of response object for currency conversion. Null values are excluded.
 *
 * @author b.mert.cotuk
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConversionSimpleResponse {

    private UUID transactionId;
    private Double targetAmount;
}
