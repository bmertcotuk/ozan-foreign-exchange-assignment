package com.ozan.foreignexchangeapi.model.dto.internal;

import lombok.Data;

/**
 * A simple response object for exchange rate operations.
 *
 * @author b.mert.cotuk
 */
@Data
public class ExchangeRateResponse {

    private Double exchangeRate;
}
