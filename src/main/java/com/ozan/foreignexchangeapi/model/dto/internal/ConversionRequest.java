package com.ozan.foreignexchangeapi.model.dto.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * A request object for currency conversion operation. Null values are excluded.
 *
 * @author b.mert.cotuk
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConversionRequest {

    private String sourceCurrency;
    private Double sourceAmount;
    private String targetCurrency;
}
