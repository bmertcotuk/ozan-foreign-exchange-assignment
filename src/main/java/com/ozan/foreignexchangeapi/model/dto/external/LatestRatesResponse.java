package com.ozan.foreignexchangeapi.model.dto.external;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * A response object to map the response returned from Data Fixer API's Latest Rates endpoint.
 *
 * @author b.mert.cotuk
 */
@Data
@NoArgsConstructor
public class LatestRatesResponse {

    private boolean success;
    private Long timestamp;
    private String base;
    private String date;
    private Map<String, Double> rates;
    private Error error;

    @Data
    @NoArgsConstructor
    public class Error {
        private Long code;
        private String info;
    }
}
