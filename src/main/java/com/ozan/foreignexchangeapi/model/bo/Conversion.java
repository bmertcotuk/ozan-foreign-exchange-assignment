package com.ozan.foreignexchangeapi.model.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Currency conversion object which serves as BO and entity.
 *
 * @author b.mert.cotuk
 */
@Data
@Entity
@NoArgsConstructor
public class Conversion implements Serializable {

    private static final long serialVersionUID = -8780775924525114847L;

    @Id
    @GeneratedValue
    private Long internalId;

    @Column(unique = true)
    private UUID transactionId;

    @Temporal(TemporalType.DATE)
    private Date transactionDate;

    private String sourceCurrency;
    private Double sourceAmount;
    private String targetCurrency;
    private Double targetAmount;

}
