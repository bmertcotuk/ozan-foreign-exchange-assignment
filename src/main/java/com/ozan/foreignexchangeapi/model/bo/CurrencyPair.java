package com.ozan.foreignexchangeapi.model.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Currency pair object which is used as a BO.
 *
 * @author b.mert.cotuk
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyPair {

    private String baseCurrency;
    private String targetCurrency;
}
