package com.ozan.foreignexchangeapi.controller;

import com.ozan.foreignexchangeapi.mapper.ConversionMapper;
import com.ozan.foreignexchangeapi.model.bo.Conversion;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionSimpleResponse;
import com.ozan.foreignexchangeapi.service.internal.CurrencyConversionService;
import com.ozan.foreignexchangeapi.service.internal.RequestValidatorService;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Date;

/**
 * The controller class for all currency conversions operations.
 *
 * @author b.mert.cotuk
 */
@RestController
@RequestMapping("/conversion")
public class CurrencyConversionControllerImpl implements CurrencyConversionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyConversionControllerImpl.class);
    private static final ConversionMapper conversionMapper = Mappers.getMapper(ConversionMapper.class);

    @Autowired
    CurrencyConversionService currencyConversionService;

    @Autowired
    RequestValidatorService requestValidatorService;

    /**
     * {@inheritDoc}
     */
    @PostMapping
    public ResponseEntity<ConversionSimpleResponse> postConversion(@RequestBody ConversionRequest conversionRequest) {

        LOGGER.debug("Conversion request has been received: {}", conversionRequest);
        requestValidatorService.validateConversionRequest(conversionRequest);
        Conversion inputConversion = conversionMapper.mapToEntity(conversionRequest);
        Conversion createdConversion = currencyConversionService.convertAndStoreConversion(inputConversion);
        LOGGER.debug("Persisted the conversion into DB: {}", createdConversion);

        ConversionSimpleResponse conversionSimpleResponse = conversionMapper.mapToSimpleResponse(createdConversion);
        URI location = URI.create(String.format("/%s", conversionSimpleResponse.getTransactionId()));

        return ResponseEntity.created(location).body(conversionSimpleResponse);
    }

    /**
     * {@inheritDoc}
     */
    @GetMapping
    public ResponseEntity<Page<ConversionResponse>> getConversionList(@RequestParam(required = false) String transactionId,
                                                                      @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date transactionDate,
                                                                      @RequestParam(defaultValue = "0") Integer pageNo,
                                                                      @RequestParam(defaultValue = "10") Integer pageSize) {

        LOGGER.debug("Getting the conversion list for transaction id {}, transaction date {}, page number {}, and page size {}.", transactionId, transactionDate, pageNo, pageSize);
        requestValidatorService.validateConversionListRequest(transactionId, transactionDate);
        Page<Conversion> conversionPage = currencyConversionService.retrieveConversionList(transactionId, transactionDate, pageNo, pageSize);
        Page<ConversionResponse> conversionResponsePage = conversionPage.map(conversionMapper::mapToResponse);
        return ResponseEntity.ok().body(conversionResponsePage);
    }
}
