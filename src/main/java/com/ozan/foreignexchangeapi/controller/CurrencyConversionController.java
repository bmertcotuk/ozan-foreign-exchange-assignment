package com.ozan.foreignexchangeapi.controller;

import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionSimpleResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Date;

/**
 * The controller interface for all currency conversions operations.
 *
 * @author b.mert.cotuk
 */
public interface CurrencyConversionController {

    /**
     * Calls the Data Fixer API to retrieve the latest rate between the currencies. Multiplies this rate with the amount of source currency to have the target amount. Stores the conversion in database. Errors are thrown if the request body is missing/invalid.
     *
     * @param conversionRequest
     * @return simple currency conversion response (target currency, transaction id)
     */
    ResponseEntity<ConversionSimpleResponse> postConversion(ConversionRequest conversionRequest);

    /**
     * Queries the database to retrieve the conversion records page by page. Paging has its default parameters. Errors are thrown if the query parameters are missing/invalid.
     *
     * @param transactionId
     * @param transactionDate
     * @param pageNo
     * @param pageSize
     * @return page of currency conversions
     */
    ResponseEntity<Page<ConversionResponse>> getConversionList(String transactionId,
                                                               Date transactionDate,
                                                               Integer pageNo,
                                                               Integer pageSize);
}
