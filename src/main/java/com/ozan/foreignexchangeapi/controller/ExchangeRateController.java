package com.ozan.foreignexchangeapi.controller;

import com.ozan.foreignexchangeapi.model.dto.internal.ExchangeRateResponse;
import org.springframework.http.ResponseEntity;

/**
 * The controller interface for all exchange rate operations.
 *
 * @author b.mert.cotuk
 */
public interface ExchangeRateController {

    /**
     * Calls Data Fixer API service and returns the exchange rate between the currencies using the comma separated query parameter (e.g. currencyPair=EUR,USD).
     *
     * @param currencyPairString
     * @return exchange rate
     */
    ResponseEntity<ExchangeRateResponse> getExchangeRate(String currencyPairString);
}
