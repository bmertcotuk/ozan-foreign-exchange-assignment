package com.ozan.foreignexchangeapi.controller;

import com.ozan.foreignexchangeapi.model.dto.internal.ExchangeRateResponse;
import com.ozan.foreignexchangeapi.service.internal.ExchangeRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller class for all exchange rate operations.
 *
 * @author b.mert.cotuk
 */

@RestController
@RequestMapping("/exchangeRate")
public class ExchangeRateControllerImpl implements ExchangeRateController {

    @Autowired
    ExchangeRateService exchangeRateService;

    /**
     * {@inheritDoc}
     */
    @Override
    @GetMapping
    public ResponseEntity<ExchangeRateResponse> getExchangeRate(@RequestParam(name = "currencyPair") String currencyPairString) {

        ExchangeRateResponse exchangeRateForCurrencyPair = exchangeRateService.getExchangeRateForCurrencyPair(currencyPairString);
        return ResponseEntity.ok().body(exchangeRateForCurrencyPair);
    }
}
