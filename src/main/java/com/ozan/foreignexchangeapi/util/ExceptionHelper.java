package com.ozan.foreignexchangeapi.util;

import com.ozan.foreignexchangeapi.enums.ApplicationError;
import com.ozan.foreignexchangeapi.exception.DataFixerApiException;
import com.ozan.foreignexchangeapi.exception.InvalidCurrencyPairException;
import com.ozan.foreignexchangeapi.exception.MissingParameterException;
import com.ozan.foreignexchangeapi.model.dto.internal.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * The helper class to handle all exceptions in a global and readable way across the application.
 *
 * @author b.mert.cotuk
 */
@ControllerAdvice
public class ExceptionHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHelper.class);
    private static final String ERROR_TEMPLATE = "Request failed with the following error: {}";

    /**
     * Creates an error response object for InvalidCurrencyPairException. Logs this response and returns it.
     *
     * @param e
     * @return error response
     */
    @ExceptionHandler(InvalidCurrencyPairException.class)
    public ResponseEntity<ErrorResponse> handleInvalidCurrencyPairException(InvalidCurrencyPairException e) {
        ErrorResponse errorResponse = new ErrorResponse(ApplicationError.INVALID_CURRENCY_PAIR_ERROR, e.getMessage());
        LOGGER.error(ERROR_TEMPLATE, errorResponse);
        return ResponseEntity.badRequest().body(errorResponse);
    }

    /**
     * Creates an error response object for MissingParameterException. Logs this response and returns it.
     *
     * @param e
     * @return error response
     */
    @ExceptionHandler(MissingParameterException.class)
    public ResponseEntity<ErrorResponse> handleMissingParameterException(MissingParameterException e) {
        ErrorResponse errorResponse = new ErrorResponse(ApplicationError.MISSING_PARAMETER_ERROR, e.getMessage());
        LOGGER.error(ERROR_TEMPLATE, errorResponse);
        return ResponseEntity.badRequest().body(errorResponse);
    }

    /**
     * Creates an error response object for DataFixerApiException. Logs this response and returns it.
     *
     * @param e
     * @return error response
     */
    @ExceptionHandler(DataFixerApiException.class)
    public ResponseEntity<ErrorResponse> handleDataFixerApiException(DataFixerApiException e) {
        ErrorResponse errorResponse = new ErrorResponse(ApplicationError.DATA_FIXER_API_ERROR, e.getMessage());
        LOGGER.error(ERROR_TEMPLATE, errorResponse);
        return ResponseEntity.badRequest().body(errorResponse);
    }

    /**
     * Creates an error response object for other exceptions. Logs this response and returns it.
     *
     * @param e
     * @return error response
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleOtherException(Exception e) {
        ErrorResponse errorResponse = new ErrorResponse(ApplicationError.GENERIC_ERROR, e.getMessage());
        LOGGER.error(ERROR_TEMPLATE, errorResponse);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }
}
