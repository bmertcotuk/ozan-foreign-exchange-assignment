package com.ozan.foreignexchangeapi.util;

import com.ozan.foreignexchangeapi.exception.InvalidCurrencyPairException;
import com.ozan.foreignexchangeapi.model.bo.CurrencyPair;
import com.ozan.foreignexchangeapi.model.dto.external.LatestRatesResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ExchangeRateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The utility class with some handy general-purpose methods.
 *
 * @author b.mert.cotuk
 */
public class CommonUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtils.class);

    private CommonUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Trims and splits the currency pair string by comma (,) and returns a currency pair with this information. Throws an error if format of the string is not valid.
     *
     * @param currencyPairString
     * @return currency pair
     */
    public static CurrencyPair getCurrencyPair(String currencyPairString) {

        LOGGER.info("Tokenizing the currency pair from the string {}", currencyPairString);
        String[] currencyArray = currencyPairString.split("\\s*,\\s*");
        if (currencyArray.length != 2) {
            throw new InvalidCurrencyPairException();
        }

        return new CurrencyPair(currencyArray[0], currencyArray[1]);
    }

    /**
     * Creates and returns an exchange rate response using the Data Fixer API's response and currency pair. Not part of any mapper because of the dependency on currency pair.
     *
     * @param latestRatesResponse
     * @param currencyPair
     * @return exchange rate response
     */
    public static ExchangeRateResponse exchangeRateFromLatestRatesResponse(LatestRatesResponse latestRatesResponse, CurrencyPair currencyPair) {
        ExchangeRateResponse internalDto = new ExchangeRateResponse();
        internalDto.setExchangeRate(latestRatesResponse.getRates().get(currencyPair.getTargetCurrency()));
        return internalDto;
    }
}

