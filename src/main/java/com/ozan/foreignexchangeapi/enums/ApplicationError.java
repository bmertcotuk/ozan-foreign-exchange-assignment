package com.ozan.foreignexchangeapi.enums;

/**
 * The enum for application related errors with  error code and description information.
 *
 * @author b.mert.cotuk
 */
public enum ApplicationError {

    MISSING_PARAMETER_ERROR(10006, "Missing Parameter"),
    INVALID_CURRENCY_PAIR_ERROR(10007, "Invalid Currency Pair"),
    DATA_FIXER_API_ERROR(10008, "Data Fixer API Call Failure"),
    GENERIC_ERROR(10009, "Generic Application Error");

    private final int code;
    private final String description;

    private ApplicationError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }
}