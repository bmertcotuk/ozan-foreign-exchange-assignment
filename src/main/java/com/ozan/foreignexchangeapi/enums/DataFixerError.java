package com.ozan.foreignexchangeapi.enums;

/**
 * The enum for Data Fixer API related errors with error code and description information.
 *
 * @author b.mert.cotuk
 */
public enum DataFixerError {

    RESOURCE_NOT_FOUND(404, "The requested resource does not exist."),
    INVALID_API_KEY(101, "No API Key was specified or an invalid API Key was specified."),
    INVALID_API_ENDPOINT(103, "The requested API endpoint does not exist."),
    MONTHLY_REQUEST_LIMIT_EXCEEDED(104, "The maximum allowed API amount of monthly API requests has been reached."),
    ENDPOINT_NOT_ALLOWED(105, "The current subscription plan does not support this API endpoint."),
    NO_RESULTS(106, "The current request did not return any results."),
    USER_ACCOUNT_INACTIVE(102, "The account this API request is coming from is inactive."),
    INVALID_BASE_CURRENCY(201, "An invalid base currency has been entered."),
    INVALID_SYMBOLS(202, "One or more invalid symbols have been specified."),
    NO_DATE_SPECIFIED(301, "No date has been specified. [historical]"),
    INVALID_OR_MISSING_DATE(302, "An invalid date has been specified. [historical, convert]"),
    INVALID_OR_MISSING_AMOUNT(403, "No or an invalid amount has been specified. [convert]"),
    INVALID_OR_MISSING_TIME_FRAME(501, "No or an invalid timeframe has been specified. [timeseries]"),
    INVALID_START_DATE(502, "No or an invalid \"start_date\" has been specified. [timeseries, fluctuation]"),
    NO_END_DATE(503, "No or an invalid \"end_date\" has been specified. [timeseries, fluctuation]"),
    INVALID_TIME_FRAME(504, "An invalid timeframe has been specified. [timeseries, fluctuation]"),
    TIMEFRAME_TOO_LONG(505, "The specified timeframe is too long, exceeding 365 days. [timeseries, fluctuation]");

    private final int code;
    private final String description;

    private DataFixerError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    public static DataFixerError fromCode(Long code) {
        for (DataFixerError dataFixerError : DataFixerError.values()) {
            if (dataFixerError.getCode() == code) {
                return dataFixerError;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }
}