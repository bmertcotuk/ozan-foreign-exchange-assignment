package com.ozan.foreignexchangeapi.exception;

/**
 * Exception class for the case when currency pair information is provided incorrectly.
 *
 * @author b.mert.cotuk
 */
public class InvalidCurrencyPairException extends RuntimeException {
    public InvalidCurrencyPairException() {
        super("Currency pair should have exactly 2 comma separated valid currency symbols.");
    }
}
