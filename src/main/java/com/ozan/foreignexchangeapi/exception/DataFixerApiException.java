package com.ozan.foreignexchangeapi.exception;

/**
 * Exception class for Data Fixer API errors.
 *
 * @author b.mert.cotuk
 */
public class DataFixerApiException extends RuntimeException {
    public DataFixerApiException(String message) {
        super(message);
    }
}
