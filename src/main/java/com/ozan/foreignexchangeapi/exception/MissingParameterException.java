package com.ozan.foreignexchangeapi.exception;

/**
 * Exception class for the case when a parameter is missing in some part of the request.
 *
 * @author b.mert.cotuk
 */
public class MissingParameterException extends RuntimeException {
    public MissingParameterException(String message) {
        super(message);
    }
}
