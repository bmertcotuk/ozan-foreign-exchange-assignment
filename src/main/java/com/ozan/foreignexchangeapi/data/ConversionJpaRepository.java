package com.ozan.foreignexchangeapi.data;

import com.ozan.foreignexchangeapi.model.bo.Conversion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The JPA repository interface for currency conversion DB operations.
 *
 * @author b.mert.cotuk
 */
public interface ConversionJpaRepository extends JpaRepository<Conversion, Long> {
}
