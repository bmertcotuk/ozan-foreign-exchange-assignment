package com.ozan.foreignexchangeapi.mapper;

import com.ozan.foreignexchangeapi.model.bo.Conversion;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionRequest;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionResponse;
import com.ozan.foreignexchangeapi.model.dto.internal.ConversionSimpleResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * Mapper interface to map different types of currency conversion objects.
 *
 * @author b.mert.cotuk
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ConversionMapper {

    ConversionResponse mapToResponse(Conversion conversion);

    ConversionSimpleResponse mapToSimpleResponse(Conversion conversion);

    Conversion mapToEntity(ConversionRequest conversionRequest);
}
