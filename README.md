# Ozan.com - Technical Challenge
## Foreign Exchange Application

This file explains the implementation of the project.

### Technologies Used
* Java 11
* Spring Boot (Web, Data JPA, Dev Tools)
* REST API
* H2 Database
* Gradle
* Mapstruct
* Lombok
* Junit
* Mockito
* SpringDoc - Open API

### Functional Requirements
* All APIs have been implemented.
* External service provider https://fixer.io/ has been used with its limitations for a free subscription.
* Error codes, descriptions, and messages have been provided to guide the user in error cases.

### Technical Requirements
* Application does not require any external configuration to be run.
* A RESTful API has been implemented by using Spring Boot.
* Gradle has been used for dependency management and build.
* Interfaces have been separated from the classes for service.
* Code structure has been organized according to separation of concerns.
* Internal database primary keys have been separated from transaction ids which are UUIDs.
* Unit tests have been provided for each class.
* Lombok has been used to get rid of the boiler plate code.
* Mapstruct has been used for different mapping purposes between BOs and DTOs.
* The code has been documented with Javadoc.
* The code quality has been ensured with SonarLint.
* ExceptionHandler & ControllerAdvice have been used to handle application exceptions in a readable and global way.

### Optional Requirements
* API documentation has been provided which can be found here when the application is running: http://localhost:8080/swagger-ui-custom.html

